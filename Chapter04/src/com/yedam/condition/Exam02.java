package com.yedam.condition;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
		//학점 계산하기
		//if문으로 해결
		//사용자가 입력한 점수를 토대로 학점을 출력
		//90 이상은 A
		//90~80  B
		//80~70 C
		//70~60 D
		//그 외는 F
		
		Scanner sc= new Scanner(System.in);
		System.out.println("성적입력>");
		int score = Integer.parseInt(sc.nextLine());
		//switch문은 부등호 못 씀.
		
//		int number = score / 10;
		
		
//		switch(number) {
//		case 10:
//			System.out.println("A");
//			break;		
//		case 9:
//			System.out.println("A");
//			break;
//		case 8:
//			System.out.println("B");
//			break;
//		case 7:
//			System.out.println("C");
//			break;
//		case 6:
//			System.out.println("D");
//			break;
//		default:
//			System.out.println("F");
//			break;
//		}
		
		
		switch(score/10) {
		case 10:// 100/10 => 10  실행문을 넣지 않으면 break가 없어서 9의 실행문을 갇게 됨.
		case 9:
			System.out.println("A");
			break;
		case 8:
			System.out.println("B");
			break;
		case 7:
			System.out.println("C");
			break;
		case 6:
			System.out.println("D");
			break;
		default:
			System.out.println("F");
			break;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
} 
	}
	
	
	
	
	
	
	
