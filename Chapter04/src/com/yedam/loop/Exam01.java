package com.yedam.loop;

public class Exam01 {
	public static void main(String[] args) {
		
		int sum = 0;
		//1) 규칙
//		sum = sum + 1; //sum = 0 + 1
//		sum = sum + 2; //sum = 1 + 2
//		sum = sum + 3; //sum = 
//		sum = sum + 4; //sum = 
//		sum = sum + 5;
		
		
		//1~5까지의 합을 구하는 반복문.
		for (int i = 1; i <= 5; i ++) {
			sum = sum + i;
			}
		
		//1~100 사이의 짝수 구하는 반복문.
		//1) 규칙
		//2 4 6 8 10 12...
		// i % 2 == 0
		for (int i = 1; i<=100; i++) {
			if (i % 2 == 0) {
				System.out.println(i);
			}
		}
		
		// 1 ~ 100 사이의 홀수 구하는 반복문.
		// 1) 규칙
		for(int i =100; i >=1; i--) {
			if (i % 2 == 1) {
				System.out.println(i);
			}
		}
		
		// 1~100사이의 2의 배수 또는 3의 배수 찾기.(||)
		// 1~100사이의 2의 배수 면서 3의 배수 찾기.(&&) => 6의배수
		// 규칙
		// 2 4 6 8 10 12....
		// i % 2 == 0
		// 3의 배수 
		// i % 3 == 0
//		for (int i = 0; i <=100; i ++) {
//			if(i % 2 == 0 || i % 3 == 0 )
//				System.out.println(i +"는 2의 배수 또는 3의 배수입니다.");
//		}
		
		for (int i = 1; i <=100; i ++) {
			if(i % 2 == 0 && i % 3 == 0 )
				System.out.println(i +"는 2의 배수 이면서 3의 배수입니다.");
		}
		
		//구구단 출력
		//만약, 2단 출력
		// 2*1, 2*2, 2*3....2*9
		// 2*i
		for(int i = 1; i < 10; i++ ) {
			System.out.println("2 x " + i +" = " + (2*i));
		}
		
		// for문 안에 for문 (중첩for문
		//초기화식에 들어가는 변수 두개를 고려
		//구구단 출력
		
		for(int i = 2; i<=9; i++) {
			// i = 2 일때
			//아래 반복문은 9번 돌아감.
			for(int j =1; j<=9; j++) {
				System.out.println(i + "x" + j + "=" +(i*j));
			}
		}
		
		
		//공포의 별 찍기

		
		//*****
		//*****
		//*****
		//*****
		//*****
		
		 
		for(int i = 0; i <5; i++) {
			String star = "";  // 데이터가 없으면 비워주는 역할
			//별을 만드는 반복문
			for(int j = 0; j<5; j++) {
				// +연산자를 사용하여 *****을 만듬.
				 star = star + "*";
			}
			System.out.println(star);
		}
		
		
		//*
		//**
		//***
		//****
		//*****
		
		for(int i=1; i<=5; i++) {
			String star = "";
			for(int j=0; i>j; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}
		
	
		
		//*****
		//****
		//***
		//**
		//*		
		
		for(int i=5; i>=1; i--) {
			String star = "";
			for(int j=0; i>j; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}
		//위랑 같음.
//		for(int i=5; i>0; i--) {
//			String star = "";
//			for(int j=i; j>0; j11) {
//				star = star + "*";
//			}
//			System.out.println(star);
//		}
//		
		//    *
		//   **
		//  ***
		// ****
		//*****
		
		for(int i=5; i>0; i--) {
			String star = "";
			for(int j=1; j<=5; j++) {
				if(j<i) {
					star = star + " ";
					}else if(j>=i) {
						star = star + "*";
					}
				}System.out.println(star);
			
			}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
