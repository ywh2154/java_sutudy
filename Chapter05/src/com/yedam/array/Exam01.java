package com.yedam.array;

public class Exam01 {
	public static void main(String[] args) {
		int[] intArry = {1,2,3,4,5,6};
		//int[] intArry = new int[6];
		//intArry[0] = 1
		//intArry[0] = 2 ...
		
		String[] strArry = new String[10];
		
		int[] intArry2;
		intArry2 = new int[5];
		
		int[] scores = {83,90,87};
		
		System.out.println("scores 첫번째 값 : "  + scores[0]);
		System.out.println("scores 세번째 값 : "  + scores[2]);
		
		//반복문과 배열
		int sum = 0;
		for(int i =0; i<3; i++) {
			System.out.println(scores[i]);
			sum = sum  + scores[i];
		}
		System.out.println("총 합계 : " + sum);
		
		//1 ) new 연산자를 활용해서 배열 만들기
		
		int[] point;
		point = new int[] {83,90,87};
		
		sum = 0;
		for(int i =0; i<3; i++) {
			System.out.println(point[i]);
			sum = sum + point[i];
		}
		System.out.println("총합계 : " + sum);
		
		//2 ) 
		int[] arr1 = new int[3];
		for(int i= 0; i<3; i++) {
			System.out.println("arr1["+i+"] : " + arr1[i]); //정수 참조타입은 배열안 데이터를 넣지 않았을때 0
		}
		
		arr1[0]  = 10;
		arr1[1]  = 20;
		arr1[2]  = 30;
		
		for(int i= 0; i<3; i++) {
			System.out.println("arr1["+i+"] : " + arr1[i]); 
		}
		
		
		
		double[] arr2 = new double[3];

		for(int i= 0; i<3; i++) {
			System.out.println("arr2["+i+"] : " + arr2[i]); //더블의 디폴트 값은 0.0
		}
		
		arr2[0] = 0.1;
		arr2[1] = 0.2;
		arr2[2] = 0.3;
		for(int i= 0; i<3; i++) {
			System.out.println("arr2["+i+"] : " + arr2[i]); 
		}
		
		
		String[] arr3 = new String[3];
		for(int i= 0; i<3; i++) {
			System.out.println("arr3["+i+"] : " + arr3[i]); //String 타입의 디폴트는 null
		}
		
		arr3[0] = "3월";
		arr3[1] = "11월";
		arr3[2] = "12월";
		for(int i= 0; i<3; i++) {
			System.out.println("arr3["+i+"] : " + arr3[i]); 
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
