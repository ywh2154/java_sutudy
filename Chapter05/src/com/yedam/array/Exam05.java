package com.yedam.array;

import java.util.Scanner;

public class Exam05 {
	public static void main(String[] args) {
		boolean run = true;
		int studentNum = 0;
		int[] scores =  null;
		Scanner sc = new Scanner(System.in);
		
		
		while(run) {
			System.out.println("----------------------------------------------------------------");
			System.out.println("1. 학생수 | 2. 점수입력 | 3.점수리스트 | 4.분석 | 5.종료");
			System.out.println("----------------------------------------------------------------");
			System.out.println("선택>");
			
			int selectNo = Integer.parseInt(sc.nextLine());
			
			if(selectNo == 1) {
				//배열 크기(방의 개수 )지정
				System.out.println("학생 수는?>");
				studentNum = Integer.parseInt(sc.nextLine());
				//배열크기 설정 완료
				//scores = new int[studentNum];
				
			} else if(selectNo == 2) {
				scores = new int[studentNum];
				//점수입력
				for(int i = 0; i< scores.length; i++) {
					System.out.println("Scores[" + i + "]>");
					scores[i] = Integer.parseInt(sc.nextLine());
				}
				
			} else if(selectNo == 3) {
				//점수리스트 출력
				for(int i = 0; i< scores.length; i++) {
					System.out.println("Scores[" + i + "]>"+ scores[i]);
				}
			}else if(selectNo == 4) {
				//최대값, 평균 구하기
				
				int sum = 0;
				int max = scores[0];
				for(int i = 0; i<scores.length; i++) {
					if(max<scores[i]) {
						max = scores[i];
					}
					sum = sum + scores[i];
				} 
				System.out.println("최고 점수 : " + max);
				System.out.println("평균 점수 : " + (double)sum/scores.length);
				
			}else if(selectNo == 5) {
				
				run = false;
				
			}
			
			
		}
		System.out.println("프로그램 종료.");
		
		
	}
}
