package com.yedam.reference;
//메모리의 이해...데이터타입. 기본타입, 참조타입
public class Exam01 {
	public static void main(String[] args) {
		int intVal = 10;
		int[] array = {1,2,3,4,5,6,};
		int[] array2 = {1,2,3,4,5,6,}; //힙 영역에 데이터가 들어가 있음.
		int[] array3;   // null이라는 데이터가 들어가있음, NullPointerException...라는 오류가 발생.
		
		System.out.println(intVal);
		System.out.println(array);
		System.out.println(array2);
		
		System.out.println(array == array2); //데이터가 아니라 데이터의 주소값을 비교하기 때문에 같지 않음. false.
		//memory leak => 메모리가 누수, 메모리가 부족
		
			
	}
}
