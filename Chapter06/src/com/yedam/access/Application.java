package com.yedam.access;
//접근제한자 실험
public class Application {

	public static void main(String[] args) {
		Access access = new Access();
		
		
		//public
		access.free = "free";
		
		//protected
		access.parent = "parent";
		
		//private
		access.privacy = "privacy"; 
		
		//default
		access.basic = "basic"; //같은 패키지에 존재하면 접근이 가능하다
		
		access.free();
//		access.privacy();
		
		
		Singleton obj1 =  Singleton.getInstance();
		Singleton obj2 = Singleton.getInstance();
		//객체간 비교는 ==
	
		if (obj1 == obj2) {
			System.out.println("같은 싱글톤 객체입니다.");
		} else {
			System.out.println("다른 싱글톤 객체입니다.");
		}
		
		
		
		
	}
}
