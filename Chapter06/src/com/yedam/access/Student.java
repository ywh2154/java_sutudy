package com.yedam.access;

public class Student {

	
	

	//다음을 만족하는 student 클래스 작성
	/*이름,학과, 학년 과목별 점수.
	 *과목은  Programing , Database, OS
	 *필드들은 모두 private 설정,
	 * setter 통해 필드 초기화
	 * getter 통해 데이터 reading
	 * getter 통한 학생의 정보를 출려ㅕㄱ
	 */
	
	//필드
	private String stdName;
	private String major;
	private String stdGrade;
	private int programing;
	private int dataBase;
	private int OS;
	
	
	
	
	
	
	
	//생성자
	
	//클래스를 통한 객체를 생성할 때 수행하는 일들을 모아두는 곳.
	//필드에 대한 데이터 객체를 생성할 때 초기화 할 예정이라면
	//생성자에서 this 키워드를 활용해서 초기화 하면 됨.
	
	
	//메소드
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public void setStdGrade(String stdGrade) {
		this.stdGrade = stdGrade;
	}
	public void setPrograming(int programing) {
		this.programing = programing;
	}
	public void setDataBase(int dataBase) {
		this.dataBase = dataBase;
	}
	public void setOS(int oS) {
		OS = oS;
	}
	public String getStdName() {
		return stdName;
	}
	public String getMajor() {
		return major;
	}
	public String getStdGrade() {
		return stdGrade;
	}
	public int getPrograming() {
		return programing;
	}
	public int getDataBase() {
		return dataBase;
	}
	public int getOS() {
		return OS;
	}
	
	
	
	
	
}
