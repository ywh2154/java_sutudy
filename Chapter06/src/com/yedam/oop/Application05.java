package com.yedam.oop;
//접근제한자 다른 패키지 실험.
import com.yedam.access.Access;

public class Application05 {

	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free = "free";
		
		// protected
		access.parent = "parent";//다른 패키지에서 못 씀
		
		
		//private
		access.privacy = "privacy"; //다른 패키지에서 못 씀. access
		
		
		//default
		access.basic = "basic";
		
		
		
	}
}
