package com.yedam.oop;

public class Book {
	//필드
	String name;
	//String type; 이렇게 안 해도 됨.
	String kind = "학습서";
	int price;
	String pub;
	String isbn;
	
	//생성자
	public Book(String name, String type, int price, String pub, String isbn) {
	this.name =name;
	//this.type = type;
	this.price = price;
	this.pub = pub;
	this.isbn = isbn;
	
	}
	
	//메소드
	
	void getInfo() {
		System.out.println();
		System.out.println("책 이름 : " + name);
		System.out.println("#내용");
		//System.out.println("1)종류 : " + type);
		System.out.println("1)종류 : " + kind);
		System.out.println("2)가격 : " + price +"원");
		System.out.println("3)출판사 : " + pub);
		System.out.println("4)도서번호: " + isbn);
	}
	
	
	
	
	
}
