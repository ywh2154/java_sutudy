package com.yedam.oop;

public class BookInfo {
	public static void main(String[] args) {
		
		Book book1 = new Book("혼자 공부하는 자바", "학습서", 24000, "한빛미디어", "yedam-001");
		book1.getInfo();
		Book book2 = new Book("이것이 리눅스다", "학습서", 32000 , "한빛미디어", "yedam-002");
		book2.getInfo();
		Book book3 = new Book("자바스크립트 파워북", "학습서", 22000, "어포스트", "yedam-003");
		book3.getInfo();
		
	}
}
