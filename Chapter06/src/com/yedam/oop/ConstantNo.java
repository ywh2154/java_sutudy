package com.yedam.oop;

public class ConstantNo {
  static final double PI = 3.13123123;
  static final int EARTH_ROUND = 46250;
  static final int LIGHT_SPEED = 127000;
  
  public ConstantNo() {
	  
  }
}
