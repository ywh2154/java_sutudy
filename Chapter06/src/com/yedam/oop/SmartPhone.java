package com.yedam.oop;

public class SmartPhone {

	//필드
		//객체의 정보를 저장
		String name;
		String maker;
		int price;
		
		
	//생성자(클래스 이름과 똑같이 부여해서 만듬)
	
	//public SmartPhone() {}    => 기본 생성자
	//자바에서 생성자가 클래스 내부에 "하나도 없을 때" 알아서 기본 생성자를
	//만들고 객체를 생성.
//	public SmartPhone() {
//		//기본생성자
//	}
		
		//this -> 내 자신, 클래스
	public SmartPhone() {
		this.name = "iphone14Pro";
	}
	public SmartPhone(int price) {
		
	}
	public SmartPhone(String name) {
		//객체를 만들때 내가 원하는 행동 또는 저장 등등
		//할때 여기에 내용을 구현
	}
	public SmartPhone(String name, int price) {
		this.name = name;
		this.price = price;
	}
		
	public SmartPhone(String name, String maker, int price)	{
		
		this.name = name; 	//Class필드의 name을 뜻함.		
		this.maker = maker;
		this.price = price;
	}
		
		
		
	//메소드 
	//객체의 기능을 정의
		void call() {
			System.out.println(name + "전화를 겁니다.");
		}
		void hangUp() {
			System.out.println(name + "전화를 끊습니다.");
		}
		
	
		
	//1)리턴 타입이 없는 경우 : void
		void getInfo(int no) {
			System.out.println("매개 변수 : " + no);
		}
		
	//2)리턴 타입이 있는 경우
	//1. 기본 타입 : int, double, long...
	//2. 참조 타입 : String, 배열, 클래스...
	//2-1)리턴 타입이 기본 타입일 경우
		int getInfo(String temp) {
			return 0;			
		}
		
		
	//2-2) 리턴 타입이 참조 타입일 경우
		String[] getInfo(String[] temp) {
			
			return temp;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}
