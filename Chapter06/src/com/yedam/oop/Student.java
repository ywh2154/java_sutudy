package com.yedam.oop;

public class Student {
	//필드
	String student;
	String school;
	int studentNo;
	int kor;
	int math;
	int eng;
	
	//생성자
	public Student(String student, String school, int studentNo, int kor, int math, int eng) {
		this.student=student;
		this.school= school;
		this.studentNo= studentNo;
		this.kor = kor;
		this.math = math;
		this.eng = eng;
		//1)생성자를 통한 필드 초기화 > 생성자(매개변수)////////////////////
	}
	public Student() {
		 //2)객체 필드에 접근하여 필드 초기화 -> 기본 생성자 이걸 만들어 줘야 함 위에 생성자 필드가 있기 때문에 
		//저절로 만들어 지지 않음...
	}
	//메소드
	void getInfo() {
		System.out.println("학생의 이름 : " + student);
		System.out.println("학생의 학교 : " + school);
		System.out.println("학생의 학번 : " + studentNo);
		System.out.println("총 점 : " + sum());
		System.out.println("평 균 : " + avgs());
	}
	
//	int plus(int kor, int math, int eng) { 굳이 매개변수를 넣을 필요가 없다.
//		int result = (kor + math + eng);
//		return result;
//	} System.out.println("총 점 : " + plus(kor,math,eng));
	
	
	
	int sum() {
		return kor+ math+ eng;
	}
	
	
//	int ave(int kor, int math, int eng) {
//		int result = plus(kor,math,eng) / 3;
//		return result;
//		
//	}
	double avgs() {
		double avgs =  sum() /(double) 3;
		return avgs;
	}
	
	
	
	
	
	
	
	
}
