package com.yedam.oop;

public class StudentInfo {
	public static void main(String[] args) {
		
		Student student1 = new Student("고길동", "예담고등학교", 221124 , 30, 40, 50);
		student1.getInfo();
		
		Student student2 = new Student();
		student2.student = "김둘리";
		student2.school = "예담고등학교";
		student2.studentNo = 221125;
		student2.kor= 50;
		student2.math=60;
		student2.eng= 70;
		student2.getInfo();
		
		Student student3 = new Student();
		student3.student = "김또치";
		student3.school = "예담고등학교";
		student3.studentNo = 221126;
		student3.kor= 50;
		student3.math=80;
		student3.eng= 70;
		student3.getInfo();
	
	}
}
