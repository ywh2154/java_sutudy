package com.yedam.inheri;

public class Parent {
//부모클래스
	
	 protected String firstName = "Lee";
	 protected String lastName;
	 protected String DNA = "B";
//	 protected char bloodType = '0';
	 int age;
	 
	 
	 //2)상속 대상에서 제외
	 private char bloodType = 'B';
	
	 //메소드
	 //오버라이딩 예제
	 protected void method1 () {
		 System.out.println("parent class -> method1 Call");
	 }
	 
	 public void method2() {
		 System.out.println("parent class -> method2 Call");
	 }
	 
}
