package com.yedam.inheri;

public class Person extends People {

	public int age;
	//자식 객체를 만들때, 생성자를 통해서 만든다.
	//super()를 통해서 부모 객체를 생성한다.
	//여기서 super() 가 의미하는 것은 부모의 생성자를 호출 하는 것이다.
	//따라서 자식 객체를 만들게 되면 부모 객체도 같이 만들어진다.
	public Person(String name, String ssn) {
		super(name, ssn);
	}
}
