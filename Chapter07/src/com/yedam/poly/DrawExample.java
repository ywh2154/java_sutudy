package com.yedam.poly;

public class DrawExample {
	public static void main(String[] args) {
		//자동타입변환
		//부모타입 변수 = new 자식클래스();
		
		Draw figure = new Circle();
		
		figure.x = 1;
		figure.y = 2;
		
		figure.draw();
		
		figure = new Rectangle();
		
		figure.draw();
		
	}
}
