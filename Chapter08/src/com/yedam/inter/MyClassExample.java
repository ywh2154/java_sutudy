package com.yedam.inter;

public class MyClassExample {

	public static void main(String[] args) {
		System.out.println("1)=================");
		
		MyClass myClass = new MyClass();
		
		myClass.rc.turnOn();
		myClass.rc.turnOff();
		
		System.out.println();
		System.out.println("2)=================");
		
		
		MyClass myClass2 = new MyClass(new Audio());
		
		
		System.out.println("3)=================");
		
		MyClass myClass3 = new MyClass();
		myClass3.method1();
		
		System.out.println("4)================="); 
		
		MyClass myClass4 = new MyClass();
		myClass4.methodB(new Television()); //매개변수를 활용...
		
		
		//인터페이스의 다형성.
		RemoteControl a = new Audio(); //객체만 바꾸면 코드 수정이 필요없음.
		
		a.turnOn();
		a.turnOff();
		
	}
}
