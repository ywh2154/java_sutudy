package com.yedam.inter;

public class RCExample {

	public static void main(String[] args) {
		RemoteControl rc;
		
		rc = new Television();
		//SmartTv 클래스 -> implements RemoteControl(+ Searchable)
		//RemoteControl(+Searchable) -> Searchable 을 상속받고 있기 때문에.
		
		//RemoteControl
		rc.turnOff();
		rc.setVolume(40);
		rc.turnOff();
		
		
	//Searchable
//		Searchable sc = new SmartTV();
		rc.search("www.google.com");
		
		rc = new Audio();
		
		rc.turnOn();
		rc.setVolume(5);
		rc.turnOff();
		
		
//		Television tv = new Television(); //단독적으로 쓸려면 이렇게 써도 되긴 함.
//		
//		tv.turnOn();
//		tv.setVolume(4);
//		tv.turnOff();
//		
	}
}
