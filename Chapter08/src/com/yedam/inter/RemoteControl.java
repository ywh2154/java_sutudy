package com.yedam.inter;

public interface RemoteControl extends Searchable {
	
	
	//상수
	public static final int MAX_VOLUME = 10; //밑에처럼 생략 가능함.
	public int MIN_VOLUME  = 0;
	
	//추상 메소드
	public void turnOn();
	public abstract void turnOff();//abstract는 생략 가능함.아래랑 위 처럼.
	public void setVolume(int volume);
	
	
	
}
