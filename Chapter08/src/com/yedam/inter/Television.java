package com.yedam.inter;

public class Television implements RemoteControl{

	//필드
	private int volume;
	
	
	//생성자
	
	//메소드
	
	
	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다");
		
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다");
		
	}

	@Override
	public void setVolume(int volume) {
		if (volume >RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		} else if(volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		}else {
			this.volume= volume;
		}
		
		System.out.println("현재 Television 볼륨 : " + volume);
	}

	@Override
	public void search(String url) {
		
	}
	
}
