package com.yedam.inter;

public class circle implements getInfo {
	
	//필드
	int radius;
	
	//생성자
	public circle(int radius) {
		this.radius = radius;
	}
	//메소드 
	
	@Override
	public void area() {
		//원 넓이 pi * R*R
		System.out.println(Math.PI *radius*radius);
	}

	@Override
	public void round() {
		//원둘레 2* pi * r
				System.out.println(2* Math.PI * radius);
	}

}
