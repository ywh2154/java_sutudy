package com.yedam.inter;

public class rectangle implements getInfo{

	//필드
	private int width;
	private int horizontal;
	
	//생성자
	
	public rectangle (int width, int horizontal) {
		this.width = width;
		this.horizontal = horizontal;
	}
	
	//메소드
	
	
	@Override
	public void area() {
		System.out.println(width*horizontal);
	}

	@Override
	public void round() {
		System.out.println(2*width + 2*horizontal);
	}

}
