package com.yedam.inter2;

public class inheri {

	public static void main(String[] args) {
		//A <- B <- D 상속관계...이건 A <- D 이기도 함.
		// A <- B
		A a = new B();
		a.info();
		
		//A <- D
		A a2 = new D();
		a2.info();
		
	}
}
