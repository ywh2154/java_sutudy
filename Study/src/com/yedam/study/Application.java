package com.yedam.study;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		//두 개의 주사위를 던졌을 때, 눈의 합이 6이 되는 모든 경우의 수를 
		//출력하는 프로그램을 구현하시오.
		/* 1,5
		 * 2,4
		 * 3,3
		 * 4,2
		 * 5,1
		 * A주사위 B 주사위
		 * A 주사위가 1일때, B 주사위가 1~6까지의 합을 구한 다음
		 * 합의 결과 6이면 내가 원하는 조건의 만족.
		 */
		
		/*숫자를 하나 입력 받아, 양수인지 음수인지 출력
		 * 단 0이면 0입니다 라고 출력해주세요.
		 */
		Scanner sc = new Scanner(System.in);
		System.out.println("숫자를 입력해주 세요>");()
		
		/*정수 두개와 연산기호 1개를 입력 받아서
		 * 연산 기호에 해당되는 계산을 수행하고 출력 하세요.
		 */
		System.out.println("정수를 입력해주세요>");
		int num = Integer.parseInt(sc.nextLine());
		System.out.println("두번째 정수를 입력해주세요>");
		int num2 = Integer.parseInt(sc.nextLine());
		System.out.println("연산 기호를 입력해주세요>");
		String buho = (sc.nextLine()); ////문자열을 받아올때는 이렇게...
		
		if(buho.equals("+")) {
			System.out.println(num + num2);
		} else if (buho.equals("-")) {
			System.out.println(num - num2);
		} else if (buho.equals("/")) {
			System.out.println(num / num2);
		} else if (buho.equals("*")) {
			System.out.println(num*num2);
		}
	}
}
