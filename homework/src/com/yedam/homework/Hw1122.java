package com.yedam.homework;

import java.util.Scanner;

public class Hw1122 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		// 문제1) 차례대로 x와 y의 값이 주어졌을 때 어느 사분면에 해당되는지 출력하도록 구현하세요.
		// 각 사분면에 해당 하는 x와 y의 값은 아래를 참조하세요.
		// 제1사분면 : x>0, y>0
		// 제2사분면 : x<0, y>0
		// 제3사분면 : x<0, y<0
		// 제4사분면 : x>0, y<0
		// 문제출처, 백준(https://www.acmicpc.net/) 14681번 문제
		
		
//		System.out.println("x의 값을 입력하시오");
//		System.out.println("입력>");		
//		int xNo = Integer.parseInt(sc.nextLine());
//		System.out.println("y의 값을 입력하시오");
//		System.out.println("입력>");		
//		int yNo = Integer.parseInt(sc.nextLine());
//		
//		if(xNo > 0 && yNo > 0) {
//			System.out.println("제1사분면 입니다.");
//		}else if(xNo < 0 && yNo > 0) {
//			System.out.println("제2사분면 입니다.");
//		}else if(xNo < 0 && yNo < 0) {
//			System.out.println("제3사분면 입니다.");
//		}else {
//			System.out.println("제4사분면 입니다.");
//		}

		
		// 문제2) 연도가 주어졌을 때 해당 년도가 윤년인지를 확인해서 출력하도록 하세요.
		// 윤년은 연도가 4의 배수이면서 100의 배수가 아닐 때 또는 400의 배수일때입니다.
		// 예를 들어, 2012년은 4의 배수이면서 100의 배수가 아니라서 윤년이며,
		// 1900년은 100의 배수이고 400의 배수는 아니기 때문에 윤년이 아닙니다.
		// HiNT : 이중 IF문 사용
		// 문제출처, 백준(https://www.acmicpc.net/) 2753번 문제
		
		
		
//		System.out.println("연도를 입력하세요.");
//		System.out.println("입력>");
//		int yearNo = Integer.parseInt(sc.nextLine());
//		
//		if(yearNo % 4 == 0 && yearNo % 100 != 0) {
//			System.out.println("입력하신 연도는 윤년 입니다.");
//		} else if(yearNo % 400 == 0) {
//			System.out.println("입력하신 연도는 윤년 입니다.");
//		} else {
//			System.out.println("입력하신 연도는 윤년이 아닙니다.");
//		}
		
		
		

		
		// 문제3) 반복문을 활용하여 up & down 게임을 작성하시오. 1~100사이에
		// 기회는 5번 이내로 맞추도록 하며, 맞추게 될 시에는 정답 공개 및 축하합니다.
		// 메세지 출력.
		// 사용자가 답안 제출 시, up, down이라는 메세지를 출력하면서 
		// 정답 유추를 할 수 있도록 한다.
		
		int udNo = (int)(Math.random() * 100 + 1 );
		System.out.println("1~100 사이의 숫자를 입력하세요.");
		System.out.println();
		
		
		
		
		

		// 문제4) 차례대로 m과 n을 입력받아 m단을 n번째까지 출력하도록 하세요.
		// 예를 들어 2와 3을 입력받았을 경우 아래처럼 출력합니다.
		// 2 X 1 = 2
		// 2 X 2 = 4
		// 2 X 3 = 6
		
		
//		
//				// 문제1) 아래와 같이 각 변수를 초기화하였을 때 각 문제에 맞게 구현하세요.
//				int x = -5;
//				int y = 10;
//				int result;
//				
//				// 1-1) 부호연산자를 이용하여 변수 x의 값을 양수로 출력하세요. 단, 변수 x의 값은 변하지 말아야한다.
//				System.out.printf("x : %d, result : %d\n", x, result);
//				
//				// 1-2) 변수 x의 값을 증가시킨 후 변수 y의 값과 더한 다음 변수 y의 값을 감소시키는 연산식을 한줄로 작성하세요.
//				System.out.printf("x : %d, y : %d, result : %d\n", x, y, result);
//
//				// 1-3) 변수 x와 y의 값을 더한 값이 5가 되도록 증감연산자를 사용하여 연산식을 한줄로 작성하세요.
//				System.out.printf("x : %d, y : %d, result : %d\n", x, y, result);
//
//				// 문제2) 아래와 같이 각 변수를 초기화하였을 때 다음 결과를 차례대로 false 와 true로 출력하도록 수정하세요.
//				int m = 10;
//				int n = 5;
//
//				//System.out.println( ( m*2 == n*4 ) || ( n<=5 ) );		
//				//System.out.println( ( m/2 > 5) && ( n%2 < 1) );
//				
//				
//				// 문제3) 각 연산식을 대입 연산자 하나로 구성된 연산식으로 수정하세요.
//				int val = 0;
//
//				//val = val + 10;
//					
//				//val = val - 5;		
//				
//				//val = val * 3;
//						
//				//val = val / 5;
//				
//				
//				// 문제4) 변수 val의 값이 양수일 경우엔 변수 값을, 아닐 경우엔 0을 담는 변수를 선언하세요. (단, 삼항연산자를 사용)
//				System.out.println(intResult);
//				
//				/* 문제 5) 다음과 같이 두 개의 정수가 주어졌을 경우 
//			  			    두 정 수 중에서 음수가 있다면 'One of a or b is negative number'를,
//			  			    만일 그렇지 않다면 'both a and b are zero or more'를 출력하도록 구현하세요. */ 
//				int a = 10;
//				int b = -8;
//				String strResult;
//				System.out.println(strResult);

	}
}
