package com.yedam.poly;

public class EmpDept extends Employee{

	//필드
	public String dept;

	
	//생산자
	
	
	//Employee 클래스를 상속한다.
	//생성자를 이용하여 값을 초기화한다.
	public EmpDept (String name, int salary , String dept) {
		super(name, salary); //부모 클래스 객체 생성.		
		this.dept = dept;
	}
	
	//메소드
	public String getDept() {
		return dept;
	}


	
	@Override
	public void getInformation() {
		super.getInformation();
		System.out.println(" 부서 : " + dept);
	}


	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
	
	
}
