package com.yedam.poly;

public class Employee {

	//필드
	public String name;
	public int salary;
	//생산자
	public Employee (String name, int salary) {
		this.name = name;
		this.salary = salary;
	}

//    Employee employee = new Employee();
	
	//메소드
	public String getName() {
		return name;
	}


	public int getSalary() {
		return salary;
	}

	
	
	
	
	
	
	public void getInformation() {
		System.out.print("이름 : " + name + " 연봉 : " + salary);
		//println 의 ln을 안 붙이면 뒤에 엔터키가 안 붙기 때문에 일렬로 정렬됨.
	}
	

	

	public void print()  {
		System.out.println("수퍼클래스");
		
	}
}
