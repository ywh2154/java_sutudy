package com.yedam.poly;



//3) ObesityInfo 클래스를 정의한다.
//- StandardWeightInfo 클래스를 상속한다.
//- 메소드는 다음과 같이 정의한다.
//(1) public void getInformation() : 이름, 키, 몸무게와 비만도를 출력하는 기능
//(2) public double getObesity() : 비만도 를 구하는 기능
//( * 비만도 : (Weight - 표준 체중)/표준체중 * 100 )
public class ObesityInfo extends StandardWeightInfo{

	
//필드	
	
//생성자	
	public ObesityInfo(String name, int weight, int height) {
		super(name, weight, height);
	}

//메소드	
	
	//(1) public void getInformation() : 이름, 키, 몸무게와 비만도를 출력하는 기능
	@Override
	public void getInformation() {
		System.out.println("이름 : " + name + "몸무게 : " + weight + "키 : " + height);
		//비만도
	}
	
	//( * 비만도 : (Weight - 표준 체중)/표준체중 * 100 )
	
	public double getObesity() {
		double bmi = (weight - getStandardWeight() / getStandardWeight() * 100);
		Object obesity = null;
		if (bmi <= 18.5) {
			obesity = "저체중";
		}else if (bmi <= 22.9) {
			obesity = "정상";
			
		}else if (bmi <24.9) {
			obesity = "과체중";
			
		}else {
			obesity = "비만";
		}
		System.out.println( "이름 : " + name + "몸무게 : " + weight + "키 : " + height + " " + obesity);
		return bmi;
		
		
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
