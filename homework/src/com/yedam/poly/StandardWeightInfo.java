package com.yedam.poly;

public class StandardWeightInfo extends Human{
//	2) StandardWeightInfo 클래스를 정의한다.
//- Human 클래스를 상속한다.
//- 메소드는 다음과 같이 정의한다.O
//(1) public void getInformation() : 이름, 키, 몸무게와 표준체중을 출력하는 기능
//(2) public double getStandardWeight() : 표준체중을 구하는 기능
//( * 표준 체중 : (Height - 100) * 0.9 )
	
		
	//필드
	
	//생성자
	public StandardWeightInfo(String name, double weight, double height) {
		super(name, weight, height);
	}
	
	//메소드
	
	
	public double getStandardWeight() {
		
		double sw = (height -100 )* 0.9;
		return sw;
	}

	@Override
	public void getInformation() {
		super.getInformation();
		
		//1번방식
//		System.out.println(" 표준체중" + getStandardWeight() + "입니다.");
		//2번방식
		System.out.printf("표준체중 %.1f dlqslek.\n", getStandardWeight());
		
	}
	
	
	
}


